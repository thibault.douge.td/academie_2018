package com.example.tdouge.myapplication;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;

/**
 * Created by tdouge on 19/01/2018.
 */

public class ListFragment extends Fragment{

    final static String DATA_RECEIVE = "data_receive";

    ArrayList<String> mylist = new ArrayList<String>();
    ListView lv = (ListView)getView().findViewById(R.id.myListView);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_list_item_1, mylist){
            @Override
            public TextView getView(int position, View convertView, ViewGroup parent) {
                // Get the current item from ListView
                TextView view = (TextView)super.getView(position, convertView, parent);


                Log.w("test", "worked '" + view.getText().toString() + "' '" + cip +"'");
                if (view.getText().toString().equals(cip)) {
                    /*String tmp = mylist.get(position);
                    mylist.remove(position);
                    mylist.add(tmp); */
                    view.setBackgroundColor(Color.GREEN);
                }
//                else
//                    view.setBackgroundColor(Color.YELLOW);
                return view;
            }
        };

    public String cip;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        //Instancier vos composants graphique ici (faîtes vos findViewById)

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mylist.add("01034009387423121720030110G00176");
        mylist.add("0103400930470312171707001060267");

        lv.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();

    }


    public void displayData(String message){
        cip = message.trim();


        lv.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
//        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
//        alertDialog.setTitle("Alert");
//        alertDialog.setMessage(message);
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.show();

    }
}
