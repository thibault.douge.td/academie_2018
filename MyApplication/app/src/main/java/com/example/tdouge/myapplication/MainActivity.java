package com.example.tdouge.myapplication;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import android.graphics.Color;
import android.view.ViewGroup;
import java.util.Calendar;

import static android.support.v4.os.LocaleListCompat.create;


public class MainActivity extends Activity {

    public ArrayList<String> verif = new ArrayList<String>();
    public String result = "";
    public List<String> myList = new ArrayList<String>();
    public List<String> listEmplacement = new ArrayList<String>();
    public List<String> listDesignation = new ArrayList<String>();
    public List<String> listQuantite = new ArrayList<String>();
    int signal = 0;
    int badProduct = 0;
    boolean toRemove1 = true;
    boolean toRemove2 = true;
    boolean toRemove3 = true;
    boolean removeOne = false;
    int removePos = 3;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.decoder);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                launchDecoder();
            }});

        final ListView lvSap = (ListView) findViewById(R.id.sap);
        final ListView lvDesignation = (ListView) findViewById(R.id.designation);
        final ListView lvEmplacement = (ListView) findViewById(R.id.emplacement);
        final ListView lvQuantite = (ListView) findViewById(R.id.quantite);
        DateFormat currentTime = new SimpleDateFormat("dd/MM/yy");
        TextView header =  new TextView(this);

        String[] fruits = new String[] {
                "01034009387423121720030110G00176",
                "0103400930470312171707001060267",

        };
        myList = new ArrayList<String>(Arrays.asList(fruits));
        String[] emplacement = new String[] {
                "A",
                "B"

        };
        listEmplacement = new ArrayList<String>(Arrays.asList(emplacement));
        String[] quantite = new String[] {

                "1",
                "1"

        };
        listQuantite = new ArrayList<String>(Arrays.asList(quantite));
        String[] designation = new String[] {
                "Fusidate de Sodium",
                "HEC creme hemorroide"

        };
        listDesignation = new ArrayList<String>(Arrays.asList(designation));


        final ArrayAdapter<String> arrayDesignation = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listDesignation);
        final ArrayAdapter<String> arrayQuantite = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listQuantite);
        final ArrayAdapter<String> arrayEmplacement = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listEmplacement);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, myList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView

                TextView view = (TextView)super.getView(position,convertView,parent);

                return view;
            }
        };

        lvSap.setAdapter(arrayAdapter);
        lvDesignation.setAdapter(arrayDesignation);
        lvQuantite.setAdapter(arrayQuantite);
        lvEmplacement.setAdapter(arrayEmplacement);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            String contents = (String)data.getStringExtra("SCAN_RESULT"); //this is the result
            result = contents.trim();

        }
        toRemove1 = false;
        toRemove2 = false;
        toRemove3 = false;
        badProduct = 0;
        final ListView lvDesignation = (ListView) findViewById(R.id.designation);
        final ListView lvEmplacement = (ListView) findViewById(R.id.emplacement);
        final ListView lvQuantite = (ListView) findViewById(R.id.quantite);
        final ListView lvSap = (ListView) findViewById(R.id.sap);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, myList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView

                if (lvSap.getCount() == verif.size() && signal == 0){
                    signal = 1;
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Etat commande");
                    alertDialog.setMessage("Préparation terminée");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                TextView view = (TextView)super.getView(position,convertView,parent);
                if (view.getText().toString().equals(result) || verif.contains(view.getText().toString())) {
                    String tmp = myList.get(position);
                    if (removeOne == false){
                        removePos = position;
                        myList.remove(position);
                        myList.add(tmp);

                        toRemove1 = true;
                        toRemove2 = true;
                        toRemove3 = true;
                        removeOne = true;
                    }
                    view.setBackgroundColor(Color.GREEN);
                    if (!verif.contains(result))
                        verif.add(result);
                }


                else{
                    view.setBackgroundColor(Color.TRANSPARENT);
                    if (badProduct == 0 && !myList.contains(result)) {
                        badProduct = 1;
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("Etat commande");
                        alertDialog.setMessage("Erreur Commande");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    }
                return view;
            }
        };
        final ArrayAdapter<String> arrayDesignation = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listDesignation){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){

                TextView view = (TextView)super.getView(position,convertView,parent);
                if (toRemove1 == true && position == lvDesignation.getCount()-1) {
                    String tmp = listDesignation.get(removePos);
                    Log.w("test", " " + tmp + " " + removePos);
                    listDesignation.remove(removePos);
                    listDesignation.add(tmp);
                    toRemove1 = false;
                }
                if (position >= lvDesignation.getCount() - verif.size())
                    view.setBackgroundColor(Color.GREEN);
                else
                    view.setBackgroundColor(Color.TRANSPARENT);
                return view;
            }
        };
        final ArrayAdapter<String> arrayQuantite = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listQuantite){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){

                TextView view = (TextView)super.getView(position,convertView,parent);
                if (toRemove2 == true && position == lvDesignation.getCount()-1) {
                    String tmp = listQuantite.get(removePos);
                    listQuantite.remove(removePos);
                    listQuantite.add(tmp);
                    toRemove2 = false;
                }
                if (position >= lvDesignation.getCount() - verif.size())
                    view.setBackgroundColor(Color.GREEN);
                else
                    view.setBackgroundColor(Color.TRANSPARENT);
                return view;
            }
        };
        final ArrayAdapter<String> arrayEmplacement = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, listEmplacement){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){

                TextView view = (TextView)super.getView(position,convertView,parent);
                if (toRemove3 == true && position == lvDesignation.getCount()-1) {
                    String tmp = listEmplacement.get(removePos);
                    listEmplacement.remove(removePos);
                    listEmplacement.add(tmp);
                    toRemove3 = false;
                }
                if (position >= lvEmplacement.getCount() - verif.size())
                    view.setBackgroundColor(Color.GREEN);
                else
                    view.setBackgroundColor(Color.TRANSPARENT);
                return view;
            }
        };
        removeOne = false;
        lvSap.setAdapter(arrayAdapter);
        lvDesignation.setAdapter(arrayDesignation);
        lvQuantite.setAdapter(arrayQuantite);
        lvEmplacement.setAdapter(arrayEmplacement);
    }

    private void launchDecoder() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        intent.putExtra("SCAN_MODE", "DATA_MATRIX");//for Qr code, its "QR_CODE_MODE" instead of "PRODUCT_MODE"
        intent.putExtra("SAVE_HISTORY", false);//this stops saving ur barcode in barcode scanner app's history
        startActivityForResult(intent, 0);


    }


}
